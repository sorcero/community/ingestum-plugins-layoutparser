#
# Copyright (c) 2021 Sorcero, Inc.
#
# This file is part of Sorcero's Language Intelligence platform
# (see https://www.sorcero.com).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#


from ingestum import transformers
from ingestum import sources
from ingestum import documents
from ingestum import utils

import cv2
import os
import re

import layoutparser as lp
from vila.pdftools.pdf_extractor import PDFExtractor
from vila.predictors import HierarchicalPDFPredictor

from tempfile import TemporaryDirectory
from pdf2image import convert_from_path

__script__ = os.path.basename(__file__).replace(".py", "")
regex = re.compile("[^a-zA-Z]")


class LPTitleParser(
    transformers.pdf_source_create_publication_document.BaseTitleParser
):
    priority = 4

    def get_title(self, source):
        directory = TemporaryDirectory()

        image_path = convert_from_path(
            pdf_path=source.path,
            output_folder=directory.name,
            first_page=1,
            last_page=1,
            paths_only=True,
        )[0]
        image = cv2.imread(image_path)[..., ::-1]

        directory.cleanup()

        model = lp.Detectron2LayoutModel(
            "lp://PubLayNet/mask_rcnn_X_101_32x8d_FPN_3x/config",
            extra_config=[
                "MODEL.ROI_HEADS.SCORE_THRESH_TEST",
                0.5,
                "MODEL.ROI_HEADS.NMS_THRESH_TEST",
                0.05,
            ],
            label_map={0: "Text", 1: "Title", 2: "List", 3: "Table", 4: "Figure"},
        )

        blocks = model.detect(image)
        h, w = image.shape[:2]
        left_interval = lp.Interval(0, w / 2 * 1.05, axis="x").put_on_canvas(image)

        ocr_agent = lp.TesseractAgent(languages="eng")

        title_blocks = lp.Layout([b for b in blocks if b.type == "Title"])

        if len(title_blocks) > 0:
            left_blocks = title_blocks.filter_by(left_interval, center=True)
            left_blocks = sorted(left_blocks, key=lambda b: b.coordinates[1])

            right_blocks = [b for b in title_blocks if b not in left_blocks]
            right_blocks = sorted(right_blocks, key=lambda b: b.coordinates[1])

            title_blocks = lp.Layout(
                [b.set(id=idx) for idx, b in enumerate(left_blocks + right_blocks)]
            )

            enriched_blocks = []

            for block in title_blocks:
                segment_image = block.crop_image(image)
                text = (
                    ocr_agent.detect(segment_image)
                    .replace("\n", " ")
                    .replace(" \x0c", "")
                )
                word_count = len(text.split())

                if word_count < 6 or word_count > 19:
                    continue
                block.set(text=text, inplace=True)
                enriched_blocks.append(
                    {"height": block.height, "text": text, "score": block.score}
                )

            if len(enriched_blocks) > 0:
                enriched_blocks.sort(
                    key=lambda block: (block["height"], block["score"]), reverse=True
                )
                return utils.sanitize_string(
                    re.sub(" +", " ", enriched_blocks[0]["text"])
                )

        # If the 'title' blocks were too short/long, maybe they don't really contain the
        # title (Example: RM - Myopericarditis following mRNA COVID-19 Vaccination in
        # Adolescents 12 through 18 Years of Age)
        text_blocks = lp.Layout([b for b in blocks if b.type == "Text"])

        if len(text_blocks) > 0:
            left_blocks = text_blocks.filter_by(left_interval, center=True)
            left_blocks = sorted(text_blocks, key=lambda b: b.coordinates[1])

            right_blocks = [b for b in text_blocks if b not in left_blocks]
            right_blocks = sorted(text_blocks, key=lambda b: b.coordinates[1])

            text_blocks = lp.Layout(
                [b.set(id=idx) for idx, b in enumerate(left_blocks + right_blocks)]
            )

            enriched_blocks = []

            for block in text_blocks:
                segment_image = block.crop_image(image)
                text = (
                    ocr_agent.detect(segment_image)
                    .replace("\n", " ")
                    .replace(" \x0c", "")
                )
                word_count = len(text.split())

                if word_count < 6 or word_count > 19:
                    continue
                block.set(text=text, inplace=True)
                enriched_blocks.append(
                    {"height": block.height, "text": text, "score": block.score}
                )

            if len(enriched_blocks) > 0:
                enriched_blocks.sort(
                    key=lambda block: (block["height"], block["score"]), reverse=True
                )
                return utils.sanitize_string(
                    re.sub(" +", " ", enriched_blocks[0]["text"])
                )

        return None


class CVStrategy(transformers.pdf_source_create_publication_document.BaseStrategy):
    """
    Uses LayoutParser and VILA to extract data from the PDF and populate the Publication fields.
    """

    priority = 3

    def augment(self, document, source):
        pdf_extractor = PDFExtractor("pdfplumber")
        page_tokens, page_images = pdf_extractor.load_tokens_and_image(source.path)

        vision_model = lp.models.EfficientDetLayoutModel(
            "lp://efficientdet/PubLayNet/tf_efficientdet_d1"
        )
        pdf_predictor = HierarchicalPDFPredictor.from_pretrained(
            "allenai/hvila-block-layoutlm-finetuned-grotoap2"
        )

        journal = ""
        doi = ""
        website = ""
        title = ""
        author = ""
        affiliation = ""
        correspondence = ""
        editor = ""
        dates = ""
        abstract = ""
        copyright = ""

        for idx, page_token in enumerate(page_tokens):
            blocks = vision_model.detect(page_images[idx])
            page_token.annotate(blocks=blocks)
            pdf_data = page_token.to_pagedata().to_dict()
            predicted_tokens = pdf_predictor.predict(pdf_data)
            break

        for i in range(len(predicted_tokens._blocks)):
            curr_type = predicted_tokens._blocks[i].type
            curr_text = predicted_tokens._blocks[i].text
            if curr_type == "BIB_INFO":
                if "doi" in curr_text:
                    doi += f"{curr_text}"
                elif "www" in curr_text:
                    website += f"{curr_text}"
                else:
                    journal += f" {curr_text}"
            elif curr_type == "TITLE":
                title += f" {curr_text}"
            elif curr_type == "AUTHOR":
                author += f" {curr_text}"
            elif curr_type == "AFFILIATION":
                affiliation += f" {curr_text}"
            elif curr_type == "CORRESPONDENCE":
                correspondence += f" {curr_text}"
            elif curr_type == "EDITOR":
                editor += f" {curr_text}"
            elif curr_type == "DATES":
                dates += f" {curr_text}"
            elif curr_type == "ABSTRACT":
                if "Abstract:" in curr_text:
                    continue
                abstract += f" {curr_text}"
            elif curr_type == "COPYRIGHT":
                copyright += f" {curr_text}"
        origin = None
        authors = author.split(",")
        for i in range(len(authors)):
            authors[i] = regex.sub("", authors[i]).strip()
            if authors[i].startswith("and"):
                authors[i] = authors[i][3:].strip()

        while "" in authors:
            authors.remove("")

        if title is not None:
            document.title = title.strip()
        if abstract is not None:
            document.abstract = abstract.strip()
        if authors is not None:
            document.authors = authors
        if journal is not None:
            document.journal = journal
        if website is not None:
            document.full_text_url = website
        if doi is not None:
            document.doi = doi
        if copyright is not None:
            document.copyright = copyright
        if origin is None:
            document.origin = "None"
        else:
            document.origin = origin
        document.type = "publication"
        return document


class Transformer(transformers.pdf_source_create_publication_document.Transformer):
    def transform(self, source: sources.PDF) -> documents.Publication:
        return super().transform(source)
