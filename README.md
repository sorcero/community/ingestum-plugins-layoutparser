# ingestum-plugins-layoutparser

## Usage
Uses LayoutParser library to detect paper layout via a fine tuned Detectron2 computer vision model. Currently uses model
to detect paper title, but will be expanded to detect first full page.

## Parameters
Plugin can be configured as needed through adjustment of the `extra_config` line in 
`pdf_source_create_publication_document.py`:
```
extra_config=["MODEL.ROI_HEADS.SCORE_THRESH_TEST", 0.5, "MODEL.ROI_HEADS.NMS_THRESH_TEST", 0.05],
```
`MODEL.ROI_HEADS.SCORE_THRESH_TEST` is the model's cutoff threshold for layout detection.
Lower values = more objects detected, but with a higher chance of error.

`MODEL.ROI_HEADS.NMS_THRESH_TEST` is the max for the detected boxes Intersection over Union value,
should be kept low to avoid the issue of having boxes detected inside of other boxes.

## Plugins directory
The directory of the plugins must be specified in an environment variable as follows.
```
export INGESTUM_PLUGINS_DIR="/path_to_your_plugins/ingestum-plugins-layoutparser/plugins/"
```

## Documentation
For more information about Ingestum plugins, see the [documentation](https://sorcero.gitlab.io/community/ingestum/plugins.html).

## Note
Some plugins have additional dependencies. Please refer to the README.md files in the individual plugin subdirectories.
