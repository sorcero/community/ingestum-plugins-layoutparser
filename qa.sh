set -e

export INGESTUM_PLUGINS_DIR=$PWD/plugins

virtualenv env > /dev/null
source env/bin/activate > /dev/null
while read line; do
    pip install $line
done < requirements.txt


pyflakes plugins/
black --check plugins/

for plugin in $INGESTUM_PLUGINS_DIR/*/ ; do

    echo "TESTING ${plugin}"

    test="${plugin}tests/test_plugin.py"
    if test -f "$test"; then
        echo RUN $test
        python3 -m pytest $test
    fi
done

echo "ALL OK"
